package com.grb.codecraftassignment.restaurantList;

import androidx.lifecycle.MutableLiveData;

import com.grb.codecraftassignment.data.model.Restaurants;
import com.grb.codecraftassignment.data.network.NetworkCallAsyncTask;
import com.grb.codecraftassignment.data.network.RequestType;
import com.grb.codecraftassignment.data.network.ResturantsDAO;

import java.util.HashMap;

public class RestaurantsRepository {
    private static RestaurantsRepository repository;
    private MutableLiveData<Restaurants[]> liveData;

    public static synchronized RestaurantsRepository getInstance() {
        if (repository == null)
            repository = new RestaurantsRepository();
        return repository;
    }

    private RestaurantsRepository() {
    }

    public MutableLiveData<Restaurants[]> getRestaurants(String latitude, String longitude) {
        liveData = new MutableLiveData<>();
        doNetworkCall(latitude,longitude);
        return liveData;
    }

    private void doNetworkCall(String latitude, String longitude) {
        String uri = "https://developers.zomato.com/api/v2.1/search?lat=" + latitude + "&lon=" + longitude;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("user-key", "fc67a2eff12ebecf96b9fd01d814b4f9");
        NetworkCallAsyncTask<Restaurants[], ResturantsDAO> task = new NetworkCallAsyncTask(uri, RequestType.GET, headers, null, new ResturantsDAO(), new NetworkCallAsyncTask.CallBack<Restaurants[]>() {

            @Override
            public void onSuccess(Restaurants[] restaurants) {
                StringBuilder sb = new StringBuilder();
                sb.append("onResponse");


                if (restaurants != null && restaurants.length > 0) {

                    for (Restaurants restaurant : restaurants) {
                        sb.append(restaurant.toString() + "\n\n");
                    }

                    liveData.postValue(restaurants);
                }


            }

            @Override
            public void onFailure(String errMsg) {

            }
        });
        task.execute();
    }

}
