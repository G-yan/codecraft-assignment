package com.grb.codecraftassignment.restaurantList;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grb.codecraftassignment.R;
import com.grb.codecraftassignment.data.model.Restaurant;
import com.grb.codecraftassignment.data.model.Restaurants;
import com.grb.codecraftassignment.restaurantPreview.RestaurantPreviewActivity;

public class ResturansAdapter extends RecyclerView.Adapter<ResturansAdapter.ViewHolder> {

    private Restaurants[] restaurants;

    public ResturansAdapter(Restaurants[] restaurants) {
        this.restaurants = restaurants;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resturant, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Restaurant restaurant = restaurants[position].getRestaurant();
        holder.resturantName.setText(restaurant.getName());
        holder.resturanAddress.setText(restaurant.getLocation().getAddress());
        Glide.with(holder.itemView.getContext()).load(restaurant.getFeatured_image()).placeholder(R.drawable.ic_code_craft).into(holder.resturantImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RestaurantPreviewActivity.class);
                intent.putExtra(RestaurantPreviewActivity.RESTAURANT_PREVIEW_URL,restaurant.getFeatured_image());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurants.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView resturantName,resturanAddress;
        ImageView resturantImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            resturantName = itemView.findViewById(R.id.tv_resturant_name);
            resturanAddress = itemView.findViewById(R.id.tv_address);
            resturantImage = itemView.findViewById(R.id.iv_resturant);
        }
    }
}
