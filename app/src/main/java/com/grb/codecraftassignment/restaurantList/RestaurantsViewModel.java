package com.grb.codecraftassignment.restaurantList;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.grb.codecraftassignment.data.model.Restaurants;

public class RestaurantsViewModel extends ViewModel {
    private MutableLiveData<Restaurants[]> liveData;

    private RestaurantsRepository repository;

    public void init(String latitude, String longitude) {
        if (liveData != null) {
            return;
        }
        repository = RestaurantsRepository.getInstance();
        liveData = repository.getRestaurants(latitude, longitude);
    }

    public MutableLiveData<Restaurants[]> getRestaurants() {
        return liveData;
    }


}
