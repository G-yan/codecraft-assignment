package com.grb.codecraftassignment.restaurantList;

import android.location.Location;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.grb.codecraftassignment.FetchLocationActivity;
import com.grb.codecraftassignment.R;
import com.grb.codecraftassignment.data.model.Restaurants;

public class MainActivity extends FetchLocationActivity {

    private TextView showLocation;
    private TextView responseText;
    private RecyclerView rvReturants;
    private RestaurantsViewModel restaurantsViewModel;
    private String latitude;
    private String longitude;

    @Override
    public void initView() {
        showLocation = findViewById(R.id.text);
        responseText = findViewById(R.id.response);
        rvReturants = findViewById(R.id.rv_resturants);
        getSupportActionBar().setTitle("Restaurants");
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewInitialised() {
        showProgress();
        getLastKnownLocation();
        restaurantsViewModel = new ViewModelProvider(this).get(RestaurantsViewModel.class);
    }

    @Override
    public void onLocationFetched(Location location) {
        hideProgress();
        if (location != null) {
            double lat = location.getLatitude();
            double longi = location.getLongitude();
            latitude = String.valueOf(lat);
            longitude = String.valueOf(longi);
            showLocation.setText("Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
        } else {
            Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
        }

        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
            showProgress();
            restaurantsViewModel.init(latitude, longitude);
            restaurantsViewModel.getRestaurants().observe(this, new Observer<Restaurants[]>() {
                @Override
                public void onChanged(Restaurants[] restaurants) {
                    hideProgress();
                    ResturansAdapter adapter = new ResturansAdapter(restaurants);
                    LinearLayoutManager layoutManager =
                            new LinearLayoutManager(MainActivity.this);
                    rvReturants.setLayoutManager(layoutManager);
                    rvReturants.setAdapter(adapter);
                }
            });

        }
    }
}