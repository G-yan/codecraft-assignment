package com.grb.codecraftassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.grb.codecraftassignment.R;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        initView();
        onViewInitialised();
    }

    public abstract void initView();
    public abstract int getContentView();
    public abstract void onViewInitialised();

    public void showProgress(){
        ProgressBar progressBar = findViewById(R.id.progress_bar);
        if(progressBar!=null)
            progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress(){
        ProgressBar progressBar = findViewById(R.id.progress_bar);
        if(progressBar!=null)
            progressBar.setVisibility(View.GONE);
    }
}