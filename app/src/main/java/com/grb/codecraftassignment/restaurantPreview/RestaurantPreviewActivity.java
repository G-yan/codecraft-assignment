package com.grb.codecraftassignment.restaurantPreview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.grb.codecraftassignment.BaseActivity;
import com.grb.codecraftassignment.R;

public class RestaurantPreviewActivity extends BaseActivity {

    public static final String RESTAURANT_PREVIEW_URL = "RestaurantPreviewUrl";

    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    private ImageView mImageView;
    private String previewUrl;

    @Override
    public void initView() {
        mImageView = (ImageView) findViewById(R.id.imageView);
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
        getSupportActionBar().hide();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_restaurant_preview;
    }

    @Override
    public void onViewInitialised() {
        previewUrl = getIntent().getStringExtra(RESTAURANT_PREVIEW_URL);
        Glide.with(this).load(previewUrl).placeholder(R.drawable.ic_code_craft).into(mImageView);

    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(1.0f,
                    Math.min(mScaleFactor, 10.0f));
            mImageView.setScaleX(mScaleFactor);
            mImageView.setScaleY(mScaleFactor);
            return true;
        }
    }
}