package com.grb.codecraftassignment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.Toast;

public abstract class FetchLocationActivity extends BaseActivity {

    private static final int REQUEST_PERMISSIONS = 231;
    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    String latitude, longitude;

    public void getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locationListener);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults[1] == PackageManager.PERMISSION_GRANTED);
                if (locationAccepted)
                    getLastKnownLocation();
                else {
                    hideProgress();
                    Toast.makeText(FetchLocationActivity.this, "You need to accept the permissions to proceed", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {
            onLocationFetched(location);
        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {
            hideProgress();
            Toast.makeText(FetchLocationActivity.this, "Please Enable GPS", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }
    };

    public abstract void onLocationFetched(Location location);
}