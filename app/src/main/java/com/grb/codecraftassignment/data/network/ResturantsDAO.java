package com.grb.codecraftassignment.data.network;

import com.google.gson.Gson;
import com.grb.codecraftassignment.data.model.Restaurants;
import com.grb.codecraftassignment.data.model.RestaurantsResponse;

public class ResturantsDAO implements INetworkDAO<Restaurants[]> {


    @Override
    public Restaurants[] getDAO(String Response) throws Exception {
        Gson g = new Gson();
        RestaurantsResponse response = g.fromJson(Response, RestaurantsResponse.class);
        if (response != null && response.getRestaurants() != null && response.getRestaurants().length > 0)
            return response.getRestaurants();
        return null;
    }
}
