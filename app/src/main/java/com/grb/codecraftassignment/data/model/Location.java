package com.grb.codecraftassignment.data.model;

public class Location
{
    private String zipcode;

    private String address;

    private String city;

    private String locality_verbose;

    private String latitude;

    private String locality;

    private String country_id;

    private String city_id;

    private String longitude;

    public String getZipcode ()
    {
        return zipcode;
    }

    public void setZipcode (String zipcode)
    {
        this.zipcode = zipcode;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getLocality_verbose ()
    {
        return locality_verbose;
    }

    public void setLocality_verbose (String locality_verbose)
    {
        this.locality_verbose = locality_verbose;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getLocality ()
    {
        return locality;
    }

    public void setLocality (String locality)
    {
        this.locality = locality;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [zipcode = "+zipcode+", address = "+address+", city = "+city+", locality_verbose = "+locality_verbose+", latitude = "+latitude+", locality = "+locality+", country_id = "+country_id+", city_id = "+city_id+", longitude = "+longitude+"]";
    }
}
