package com.grb.codecraftassignment.data.retrofit;

import com.grb.codecraftassignment.data.model.RestaurantsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CodeCraftAsgnmntService {

    @Headers({"Accept: application/json", "user-key: fc67a2eff12ebecf96b9fd01d814b4f9"})
    @GET("search")
    Call<RestaurantsResponse> getRestaurants(@Query("lat") String lat, @Query("lon") String lon);
}
