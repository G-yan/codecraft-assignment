package com.grb.codecraftassignment.data.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofit = null;
    private static String BASE_URL = "https://developers.zomato.com/api/v2.1/";

    private RetrofitInstance() {
    }

    public synchronized static CodeCraftAsgnmntService getService() {

        if (retrofit == null)
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit.create(CodeCraftAsgnmntService.class);
    }
}
