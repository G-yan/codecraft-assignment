package com.grb.codecraftassignment.data.model;

public class Restaurant
{
    private String name;

    private String featured_image;

    private Location location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeatured_image() {
        return featured_image;
    }

    public void setFeatured_image(String featured_image) {
        this.featured_image = featured_image;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+",photos_url = "+ featured_image +", "+location.toString()+"]";
    }
}
