package com.grb.codecraftassignment.data.network;

import java.util.HashMap;
import java.util.List;

public interface INetworkDAO<D> {

    public D getDAO(String Response) throws Exception;
}
