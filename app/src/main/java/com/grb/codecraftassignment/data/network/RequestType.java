package com.grb.codecraftassignment.data.network;

public interface RequestType {
    String POST = "POST";
    String GET = "GET";
    String PUT = "PUT";
    String DELETE = "DELETE";
}
