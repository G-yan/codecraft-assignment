package com.grb.codecraftassignment.data.network;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NetworkCallAsyncTask<D, T extends INetworkDAO<D>> extends AsyncTask<Void, Void, D> {

    private String urlString;
    private String requestType;
    private HashMap<String, String> requestHeaders;
    private String requestJSONBody;
    private T networkDAO;
    CallBack<D> callBack;

    public interface CallBack<D> {
        void onSuccess(D d);

        void onFailure(String errMsg);
    }

    public NetworkCallAsyncTask(String urlString, String requestType, HashMap<String, String> requestHeaders, String requestJSONBody, T networkDAO, CallBack<D> callBack) {
        this.urlString = urlString;
        this.requestType = requestType;
        this.requestHeaders = requestHeaders;
        this.requestJSONBody = requestJSONBody;
        this.networkDAO = networkDAO;
        this.callBack = callBack;
    }

    public D doNetworkCall() {
        URL url;
        HttpURLConnection urlConnection = null;
        StringBuilder sb = new StringBuilder();
        try {
            url = new URL(urlString);

            urlConnection = (HttpURLConnection) url
                    .openConnection();
            if (requestType != null && !requestType.isEmpty())
                urlConnection.setRequestMethod(requestType);

            if (requestHeaders != null && requestHeaders.size() > 0) {
                for (Map.Entry<String, String> header : requestHeaders.entrySet()) {
                    urlConnection.setRequestProperty(header.getKey(), header.getValue());
                }
            }

            if (requestJSONBody != null && !requestJSONBody.isEmpty()) {
                try (OutputStream os = urlConnection.getOutputStream()) {
                    byte[] input = requestJSONBody.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }
            }


            InputStream in = urlConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));


            String inputLine;
            {
                while ((inputLine = bufferedReader.readLine()) != null) {
                    sb.append(inputLine);
                }
            }

            if (sb != null)
                return networkDAO.getDAO(sb.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }


        return null;
    }

    @Override
    protected D doInBackground(Void... voids) {
        return doNetworkCall();
    }

    @Override
    protected void onPostExecute(D d) {
        callBack.onSuccess(d);
    }
}
