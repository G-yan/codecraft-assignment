package com.grb.codecraftassignment.data.model;

public class RestaurantsResponse
{
    private String results_found;

    private String results_shown;

    private Restaurants[] restaurants;

    private String results_start;

    public String getResults_found ()
    {
        return results_found;
    }

    public void setResults_found (String results_found)
    {
        this.results_found = results_found;
    }

    public String getResults_shown ()
    {
        return results_shown;
    }

    public void setResults_shown (String results_shown)
    {
        this.results_shown = results_shown;
    }

    public Restaurants[] getRestaurants ()
    {
        return restaurants;
    }

    public void setRestaurants (Restaurants[] restaurants)
    {
        this.restaurants = restaurants;
    }

    public String getResults_start ()
    {
        return results_start;
    }

    public void setResults_start (String results_start)
    {
        this.results_start = results_start;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [results_found = "+results_found+", results_shown = "+results_shown+", restaurants = "+restaurants+", results_start = "+results_start+"]";
    }
}